call pathogen#infect()
call pathogen#helptags()

autocmd vimenter * if !argc() | NERDTree | endif
autocmd BufNewFile,BufRead *.html.erb set filetype=html

" http://mislav.uniqpath.com/2011/12/vim-revisited/
"
set nocompatible                " choose no compatibility with legacy vi
set number                      " show line numbers
syntax enable
set encoding=utf-8
set showcmd                     " display incomplete commands
filetype plugin indent on       " load file type plugins + indentation

autocmd Filetype * AnyFoldActivate

"
" Whitespace
set autoindent                  " indent at same level of the previous line
set nowrap                      " don't wrap lines
set tabstop=2 shiftwidth=2      " a tab is two spaces (or set this to 4)
set expandtab                   " use spaces, not tabs (optional)
set smarttab                    " be smart when using tabs
set backspace=indent,eol,start  " backspace through everything in insert mode
"
" Searching
set hlsearch                    " highlight matches
set incsearch                   " incremental searching
set ignorecase                  " searches are case insensitive...
set smartcase                   " ... unless they contain at least one capital letter

" about backups and such
set wildignore=*.swp,*.bak,*.pyc,*.class  " the two latter, what for?
set nobackup
set noswapfile

set cursorline                  " highlight current line
set encoding=utf8               " set utf8 as standard encoding

set laststatus=2
set statusline=%F%m%r%h%w\ %{fugitive#statusline()}\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%l,%v][%p%%]\ 

let g:airline_powerline_fonts=1
let g:vroom_use_bundle_exec=1

" Some Linux distributions set filetype in /etc/vimrc.
" Clear filetype flags before changing runtimepath to force Vim to reload them.
if exists("g:did_load_filetypes")
  filetype off
  filetype plugin indent off
endif
set runtimepath+=$GOROOT/misc/vim " replace $GOROOT with the output of: go env GOROOT
filetype plugin indent on
syntax on

" Load matchit (% to bounce from do to end etc)
runtime! plugin/matchit.vim

nmap <silent> <Leader>p :NERDTreeToggle<cr>

colorscheme jellybeans
set background=dark

" Set a different background color after 80th column
set textwidth=80
set colorcolumn=+1
hi ColorColumn guibg=#2d2d2d ctermbg=246

" Fonts and other GUI settings
set t_Co=256
if has("gui_running")
  set guioptions-=T   " No toolbar
  set guioptions-=r   " No scrollbars except when necessary
  set guioptions+=R

  if has("gui_gtk2")
    set guifont=Droid\ Sans\ Mono\ 12
  elseif has("X11")
    set guifont=-monotype-andale_mono-medium-r-normal-*-*-*-*-c-*-*-*
  else 
    set guifont=Droid\ Sans\ Mono\ Dotted\ for\ Powerline:h16
  endif
  " tell the term has 256 colors
  set lines=40 " ?
  set columns=110
endif

